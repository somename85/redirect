const querystring = require('querystring');
const express = require('express');
const app = express();

app.get('/', (req, res) => {
  console.log('\n\n\nnext req');
  console.log(req.url);

  console.log('req.query');
  console.log(req.query);

  const stringQuery = querystring.stringify(req.query);

  console.log('stringQuery');
  console.log(stringQuery);

  const stringQueryWithoutSpaces = stringQuery.replace(/\s+/g, '').replace(/%20/g, '');

  console.log('stringQueryWithoutSpaces');
  console.log(stringQueryWithoutSpaces);

  const nextUrl = `http://offers.tinvest.affise.com/postback?${stringQueryWithoutSpaces}`;
  console.log(`redirecting to ${nextUrl}`);

  res.redirect(nextUrl);
});

app.listen(3000, () => console.log('Redirect app listening on port 3000!'));